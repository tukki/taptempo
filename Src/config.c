/**
  * Copyright (c) 2018 Joonas Ihonen
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  */


#include "eeprom.h"
#include "common.h"
#include "config.h"

struct configuration g_config;

ret_t Config_Load(void)
{
    ret_t ret;
    uint32_t words = (sizeof(g_config) + sizeof(uint32_t) - 1) / sizeof(uint32_t);
    ret = EEPROM_Read(EEPROM_ADDR_CONFIG, (uint32_t*)&g_config , words);
    if(ret == RET_OK)
    {
        if(g_config.lut_slot > CONFIG_LUT_SLOT_MAX)     g_config.lut_slot   = CONFIG_LUT_SLOT_DEFAULT;
        if(g_config.delay_select > CONFIG_DELAY_SELECT_MAX) g_config.delay_select = CONFIG_DELAY_SELECT_DEFAULT;
    }
    return ret;
}


ret_t Config_Save(void)
{
    uint32_t words = (sizeof(g_config) + sizeof(uint32_t) - 1) / sizeof(uint32_t);
    return EEPROM_Write(EEPROM_ADDR_CONFIG, (uint32_t*)&g_config , words);
}

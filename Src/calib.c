/**
  * Copyright (c) 2018 Joonas Ihonen
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  */

#include "digipot.h"
#include "calib.h"

#include "tim.h"
#include "lut.h"
#include "gpio.h"
#include "test.h"

#define CALIB__ERR_OVERRUN  0xFFFF
#define CALIB__ERR_TIMEOUT  0xFFFE
#define CALIB_TIMEOUT_MS    10000

ret_t Calib_CalibrateAll(void)
{
    uint16_t pot = 0;
    for(uint16_t i = 0; i < LUT_SIZE; i++)
    {
        if(pot != CALIB__ERR_OVERRUN)
        {
            if(i%2)
            {
                GPIO_SET_LED_ON(RED);
            }
            else
            {
                GPIO_SET_LED_OFF(RED);
            }
            pot = Calib_Calibrate(LUT_GetTime(i));
        }
        if(pot == CALIB__ERR_OVERRUN)
        {
            LUT_SetPot(i, 0x3FF);
        }
        else if(pot == CALIB__ERR_TIMEOUT)
        {
            return RET_FAILURE;
        }
        LUT_SetPot(i, pot);
    }
    GPIO_SET_LED_OFF(RED);
    return LUT_Save(GPIO_GetRotary());
}

uint16_t Calib_Calibrate(uint16_t delay_ms)
{
    static uint16_t pot_last = 0;
    uint8_t e = 0;
    uint8_t r = 0;
    int32_t errors[256] = {0};
    uint16_t res[256] = {0};

    uint16_t timeout_start;
    int16_t tolerance = delay_ms / 500;
    int16_t coeff = 2;

    int32_t delay_error = 0;
    int16_t fix = 0;
    uint16_t pot = LUT_GetPot(delay_ms);

    /* Values of the LUT should be in ascending order, if not, use previous pot value */
    if(pot < pot_last)
    {
        pot = pot_last;
    }

    while(1)
    {
        Digipot_WriteWiper(pot);
        HAL_Delay(20);
        Test_SetPulseEnabled(ON);

        timeout_start = HAL_GetTick();
        while(!g_test_signal.pulse_rx_complete)
        {
            if(HAL_GetTick() > timeout_start + CALIB_TIMEOUT_MS)
            {
                return CALIB__ERR_TIMEOUT;
            }
        }

        g_test_signal.pulse_rx_complete = 0;

        if(Tim_CheckWave())
        {
            GPIO_SET_TEST_LED_ON;
            //GPIO_SET_LED_OFF(RED);
            delay_error = (int32_t)g_test_signal.pulse_rx_time_ms - 32 - (int32_t)g_test_signal.pulse_tx_time_ms - (int32_t)delay_ms;
            if(delay_error == 0 ||
               (delay_error < 0 && delay_error > -tolerance) ||
               (delay_error > 0 && delay_error < tolerance))
            {
                break;
            }
            fix = (-delay_error) / coeff++;
            fix += delay_error > 0 ? (-1) : 1;
            pot = (uint16_t)((int16_t)pot + fix);
            res[r++] = pot;
            errors[e++] = delay_error;

            if(pot > 0x3FF)
            {
                pot = CALIB__ERR_OVERRUN;
                break;
            }
        }
        else
        {
            GPIO_SET_TEST_LED_OFF;
            //GPIO_SET_LED_ON(RED);
        }
        HAL_Delay(20);
    };

    pot_last = pot;
    return pot;
}

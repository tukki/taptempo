#include "stm32l0xx_hal_flash.h"
#include "eeprom.h"

#define EEPROM_SIZE     (DATA_EEPROM_END - DATA_EEPROM_BASE);

ret_t EEPROM_Write(uint32_t address, const uint32_t* pData, uint32_t words)
{
    uint16_t i;

    address += DATA_EEPROM_BASE;

    if(address + words * sizeof(uint32_t) > DATA_EEPROM_END)
    {
        return RET_FAILURE;
    }
    HAL_FLASHEx_DATAEEPROM_Unlock();
    for(i = 0; i < words; i++)
    {
        HAL_FLASHEx_DATAEEPROM_Program(FLASH_TYPEPROGRAMDATA_WORD, address, *pData);
        address += 4;
        pData++;
    }
    HAL_FLASHEx_DATAEEPROM_Lock();

}

ret_t EEPROM_Read(uint32_t address, uint32_t* pData, uint32_t words)
{
    uint16_t i;
    address += DATA_EEPROM_BASE;
    if(address + words * sizeof(uint32_t) > DATA_EEPROM_END)
    {
        return RET_FAILURE;
    }
    for(i = 0; i < words; i++)
    {
        *pData = *((uint32_t*)address);
        address += 4;
        pData++;
    }
    return RET_OK;
}

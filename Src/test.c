/**
  * Copyright (c) 2018 Joonas Ihonen
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  */

#include "gpio.h"
#include "tim.h"
#include "test.h"

volatile struct test_signal g_test_signal = {OFF, TEST_PULSE_LEN_INIT, TEST_PULSE_TIME_INIT, 0};

void Test_SetPulseEnabled(enum pulse_state state)
{
    if(state == ON)
    {
        if(g_test_signal.enabled == OFF)
        {
            g_test_signal.pulse_tx_time_ms = HAL_GetTick();
            DAC_Start();
            g_test_signal.counter = 0;
            g_test_signal.enabled = ON;
            GPIO_SET_TEST_LED_OFF;
        }
    }
    else
    {
        if(g_test_signal.enabled == ON)
        {
            DAC_Stop();
            g_test_signal.enabled = OFF;

            Tim_StartWaveCapture();
        }
    }
}

void Test_SetPulse(enum pulse_state state)
{
    if(state == ON)
    {
        DAC_Start();
        g_test_signal.counter = 0;
    }
    else
    {
        DAC_Stop();
    }
}

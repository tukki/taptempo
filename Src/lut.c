/**
  * Copyright (c) 2018 Joonas Ihonen
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  */

#include "eeprom.h"
#include "lut.h"

struct __packed time2pot
{
    uint16_t time;
    uint16_t pot;
};

/*
struct time2pot lut[LUT_SIZE] = {
{100,	39},
{106,	47},
{113,	55},
{120,	64},
{127,	72},
{135,	82},
{144,	94},
{153,	104},
{162,	117},
{173,	129},
{183,	139},
{195,	152},
{207,	165},
{220,	177},
{234,	191},
{248,	204},
{264,	219},
{280,	232},
{298,	247},
{316,	261},
{336,	276},
{357,	291},
{380,	307},
{403,	322},
{428,	338},
{455,	354},
{484,	371},
{514,	387},
{546,	403},
{580,	420},
{616,	436},
{655,	453},
{696,	471},
{739,	488},
{786,	505},
{835,	523},
{887,	540},
{942,	558},
{1001,	577},
{1064,	594},
{1130,	612},
{1201,	631},
{1276,	650},
{1356,	668},
{1440,	687},
{1530,	706},
{1626,	726},
{1728,	745},
{1836,	764},
{1950,	784},
{2072,	804},
{2202,	825},
{2339,	845},
{2486,	866},
{2641,	887},
{2806,	908},
{2981,	930},
};
*/

struct time2pot lut[LUT_SIZE] = {
{100,	0},
{106,	0},
{113,	0},
{120,	0},
{127,	0},
{135,	0},
{144,	0},
{153,	0},
{162,	0},
{173,	0},
{183,	0},
{195,	0},
{207,	0},
{220,	0},
{234,	0},
{248,	0},
{264,	0},
{280,	0},
{298,	0},
{316,	0},
{336,	0},
{357,	0},
{380,	0},
{403,	0},
{428,	0},
{455,	0},
{484,	0},
{514,	0},
{546,	0},
{580,	0},
{616,	0},
{655,	0},
{696,	0},
{739,	0},
{786,	0},
{835,	0},
{887,	0},
{942,	0},
{1001,	0},
{1064,	0},
{1130,	0},
{1201,	0},
{1276,	0},
{1356,	0},
{1440,	0},
{1530,	0},
{1626,	0},
{1728,	0},
{1836,	0},
{1950,	0},
{2072,	0},
{2202,	0},
{2339,	0},
{2486,	0},
{2641,	0},
{2806,	0},
{2981,	0},
};


ret_t LUT_Load(uint8_t slot)
{
    if(slot >= EEPROM_LUT_SLOTS)
    {
        return RET_INVALID_ARGUMENT;
    }
    return EEPROM_Read(EEPROM_ADDR_LUT + slot * EEPROM_LUT_SIZE, (uint32_t*)lut, LUT_SIZE);
}

ret_t LUT_Save(uint8_t slot)
{
    if(slot >= EEPROM_LUT_SLOTS)
    {
        return RET_INVALID_ARGUMENT;
    }
    return EEPROM_Write(EEPROM_ADDR_LUT + slot * EEPROM_LUT_SIZE, (uint32_t*)lut, LUT_SIZE);
}

uint16_t LUT_GetTime(uint16_t index)
{
    if(index < LUT_SIZE)
    {
        return lut[index].time;
    }
    else
    {
        return 0;
    }
}

ret_t LUT_SetPot(uint16_t index, uint16_t value)
{
    if(index < LUT_SIZE)
    {
        lut[index].pot = value;
        return RET_OK;
    }
    else
    {
        return RET_FAILURE;
    }
}

uint16_t LUT_GetPot(uint16_t time)
{
    uint32_t tmp = 0;
    uint8_t i;

    if(time < lut[0].time || time > lut[LUT_SIZE-1].time)
    {
        return 0;
    }

    for(i = 1; i < LUT_SIZE; i++)
    {
        if(time <= lut[i].time)
        {
            if(i == LUT_SIZE - 1 || time == lut[i].time)
            {
                tmp = lut[i].pot;
                break;
            }
            tmp = (uint32_t)(time - lut[i-1].time) * 65536 / (uint32_t)(lut[i].time - lut[i-1].time);
            tmp = tmp * (lut[i].pot - lut[i-1].pot);
            tmp = tmp / 65536 + lut[i-1].pot;
            break;
        }
    }
    return tmp;
}




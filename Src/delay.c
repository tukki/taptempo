/**
  * Copyright (c) 2018 Joonas Ihonen
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  */

#include "common.h"
#include "lut.h"
#include "gpio.h"
#include "digipot.h"
#include "calib.h"

#define NOTE_FULL           0
#define NOTE_DOTTED         1
#define NOTE_TRIPLET        2
#define NOTE_HALF           3
#define NOTE_HALF_TRIPLET   4
#define NOTE_QUARTER        5

ret_t Delay_Set(uint16_t delay_ms)
{
    switch(GPIO_GetRotary())
    {
    case NOTE_DOTTED:
        delay_ms = delay_ms * 3 / 4;
    break;
    case NOTE_TRIPLET:
        delay_ms = delay_ms * 2 / 3;
    break;
    case NOTE_HALF:
        delay_ms = delay_ms / 2;
    break;
    case NOTE_HALF_TRIPLET:
        delay_ms = delay_ms / 3;
    break;
    case NOTE_QUARTER:
        delay_ms = delay_ms / 4;
    break;
    }

    delay_ms = LUT_GetPot(delay_ms);

    if(!delay_ms)
    {
        return RET_FAILURE;
    }

    return Digipot_WriteWiper(delay_ms);
}

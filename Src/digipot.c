/**
  * Copyright (c) 2018 Joonas Ihonen
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  */

#include "spi.h"
#include "digipot.h"

#define CMD_NOP             0x0000
#define CMD_ENABLE_UPDATE   0x1802
#define CMD_WRITE_WIPER     0x0400
#define CMD_READ_WIPER      0x0800
#define CMD_SHUTDOWN        0x2001
#define CMD_SDO_HI_Z        0x8001

#define MASK_WIPER_VALUE    0x03FF

#define WIPER_MIN           0x000
#define WIPER_MAX           0x3FF
#define WIPER_DEFAULT       0x200

//#define REVERSE_WIPER

uint16_t g_wiper = WIPER_DEFAULT;

ret_t Digipot_EnableUpdate(void)
{
    return SPI_Transmit16(CMD_ENABLE_UPDATE);
}

ret_t Digipot_WriteWiper(uint16_t value)
{
    value &= MASK_WIPER_VALUE;
#ifdef REVERSE_WIPER
    value = WIPER_MAX - value;
#endif

    return SPI_Transmit16(CMD_WRITE_WIPER | value);
}

uint16_t Digipot_ReadWiper(void)
{
    uint16_t tmp = 0xFFFF;
    SPI_Transmit16(CMD_NOP);
    SPI_Transmit16(CMD_READ_WIPER);
    tmp = SPI_Receive16();
    return tmp;
}

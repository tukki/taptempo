/**
  * Copyright (c) 2018 Joonas Ihonen
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in all
  * copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
  * SOFTWARE.
  */

#include "gpio.h"
#include "event.h"

volatile static uint16_t s_events;

#define EVENT_ON(__event) s_events & __event

#define BUTTON_MODE_MS  20
#define ROTARY_MODE_MS  50

void Event_Handler(void)
{
    static uint16_t button_mode_counter = 0;
    static uint16_t rotary_switch_counter = 0;
    static uint8_t test_button_old = GPIO_STATE_UP;


    if(EVENT_ON(E_BUTTON_MODE))
    {
        if(button_mode_counter < BUTTON_MODE_MS)
        {
            button_mode_counter++;
        }
        else
        {
            if(g_input_mode == INPUT_TAP)
            {
                g_input_mode = INPUT_POT;
                GPIO_SET_LED_OFF(GREEN);
                GPIO_SET_LED_ON(RED);
            }
            else
            {
                g_input_mode = INPUT_TAP;
                GPIO_SET_LED_OFF(RED);
                GPIO_SET_LED_ON(GREEN);
            }
            Event_Reset(E_BUTTON_MODE);
            Event_Set(E_TEMPO_CHANGED);
        }
    }
    else
    {
        button_mode_counter = 0;
    }

    if(EVENT_ON(E_BUTTON_TAP))
    {
        Event_Reset(E_BUTTON_TAP);
    }

    if(EVENT_ON(E_BUTTON_TEST))
    {
        if(GPIO_GetButtonState() == GPIO_STATE_DOWN)
        {
            if(test_button_old == GPIO_STATE_UP)
            {
                Test_SetPulseEnabled(ON);
            }
            test_button_old = GPIO_STATE_DOWN;
        }
        else if(GPIO_GetButtonState() == GPIO_STATE_UP)
        {
                test_button_old = GPIO_STATE_UP;
        }
        Event_Reset(E_BUTTON_TEST);
    }

    if(EVENT_ON(E_ROTARY_SWITCH))
    {
        if(rotary_switch_counter < ROTARY_MODE_MS)
        {
            rotary_switch_counter++;
        }
        else
        {
            Event_Reset(E_ROTARY_SWITCH);
            Event_Set(E_TEMPO_CHANGED);
        }
    }

    if(EVENT_ON(E_TEMPO_CHANGED) && !(EVENT_ON(E_ROTARY_SWITCH)))
    {
        uint16_t tempo = g_tap_tempo;
        if(g_input_mode == INPUT_POT)
        {
            tempo = g_pot_tempo;
        }
        if(tempo >= 100 && tempo < 3000)
        {
            g_test_signal.pulse_time_ms = tempo;
            Delay_Set(tempo);
            g_led_counter = 0;
            GPIO_SET_LED_ON(BLUE);
        }
        Event_Reset(E_TEMPO_CHANGED);
    }
}


void Event_Set(uint16_t event)
{
    s_events |= event;
}


void Event_Reset(uint16_t event)
{
    s_events &= !event;
}

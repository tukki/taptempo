#ifndef TEST_H_INCLUDED
#define TEST_H_INCLUDED

#include <stdio.h>
#include "dac.h"

#define TEST_PULSE_LEN_INIT     50
#define TEST_PULSE_TIME_INIT    400


enum pulse_state
{
    OFF = 0,
    ON = 1
};

struct test_signal
{
    enum pulse_state enabled;
    uint16_t pulse_len_ms;
    uint16_t pulse_time_ms;
    uint16_t counter;
    uint32_t pulse_tx_time_ms;
    uint32_t pulse_rx_time_ms;
    uint8_t pulse_rx_complete;
};


void Test_SetPulseEnabled(enum pulse_state state);
void Test_SetPulse(enum pulse_state state);
#endif /* TEST_H_INCLUDED */

/**
  ******************************************************************************
  * File Name          : gpio.h
  * Description        : This file contains all the functions prototypes for 
  *                      the gpio  
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __gpio_H
#define __gpio_H
#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l0xx_hal.h"
#include "main.h"

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* USER CODE BEGIN Private defines */
#define BUTTON_UNSTABLE_TIME_MS 20

#define GPIO_BUTTON_UP      GPIO_PIN_SET
#define GPIO_BUTTON_DOWN    GPIO_PIN_RESET

#define GPIO_LED_OFF        GPIO_PIN_SET
#define GPIO_LED_ON         GPIO_PIN_RESET

/* Macros to set LEDs and get LED state
  __pin is e.g. LED1_OUT, LED2_OUT, BUTTON_IN
  __led is 1 or 2 */
#define GPIO_TOGGLE_PIN(__pin)              HAL_GPIO_TogglePin(__pin##_GPIO_Port, __pin##_Pin)
#define GPIO_GET_PIN_STATE(__pin)           HAL_GPIO_ReadPin(__pin##_GPIO_Port, __pin##_Pin)
#define GPIO_SET_PIN_STATE(__pin, __state)  HAL_GPIO_WritePin(__pin##_GPIO_Port, __pin##_Pin, __state)
#define GPIO_SET_LED_ON(__led)              GPIO_SET_PIN_STATE(LED_##__led##_OUT, GPIO_LED_ON)
#define GPIO_SET_LED_OFF(__led)             GPIO_SET_PIN_STATE(LED_##__led##_OUT, GPIO_LED_OFF)

#define GPIO_SET_TEST_LED_ON                GPIO_SET_PIN_STATE(LED_TEST_OUT, GPIO_PIN_SET)
#define GPIO_SET_TEST_LED_OFF               GPIO_SET_PIN_STATE(LED_TEST_OUT, GPIO_PIN_RESET)

typedef enum GPIO_state_t
{
    GPIO_STATE_UP = 0,
    GPIO_STATE_DOWN = 1,
    GPIO_STATE_UNSTABLE = 2
} GPIO_state_t;
/* USER CODE END Private defines */

void MX_GPIO_Init(void);

/* USER CODE BEGIN Prototypes */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin);
GPIO_state_t GPIO_GetButtonState(void);
uint8_t GPIO_GetRotary(void);
/* USER CODE END Prototypes */

#ifdef __cplusplus
}
#endif
#endif /*__ pinoutConfig_H */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

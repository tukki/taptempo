#ifndef LUT_H_INCLUDED
#define LUT_H_INCLUDED

#include "common.h"

#define LUT_SIZE 57

ret_t LUT_Load(uint8_t slot);
ret_t LUT_Save(uint8_t slot);

uint16_t LUT_GetTime(uint16_t index);
ret_t LUT_SetPot(uint16_t index, uint16_t value);
uint16_t LUT_GetPot(uint16_t time);

#endif /* LUT_H_INCLUDED */

#ifndef CALIB_H_INCLUDED
#define CALIB_H_INCLUDED

#include "common.h"

#define CALIB_ERROR 0xFFFF

ret_t Calib_CalibrateAll(void);
uint16_t Calib_Calibrate(uint16_t delay_ms);

#endif /* CALIB_H_INCLUDED */

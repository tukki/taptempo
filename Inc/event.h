#ifndef EVENT_H_INCLUDED
#define EVENT_H_INCLUDED

#include "common.h"

#define E_BUTTON_MODE       (0x0001)
#define E_BUTTON_TAP        (0x0002)
#define E_BUTTON_TEST       (0x0004)
#define E_ROTARY_SWITCH     (0x0008)
#define E_TEMPO_CHANGED     (0x0010)


void Event_Handler(void);

void Event_Set(uint16_t event);
void Event_Reset(uint16_t event);

#endif /* EVENT_H_INCLUDED */

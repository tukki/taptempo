#ifndef EEPROM_H_INCLUDED
#define EEPROM_H_INCLUDED

#include "common.h"

#define EEPROM_ADDR_CONFIG  0x000U
#define EEPROM_ADDR_LUT     0x100U
#define EEPROM_LUT_SIZE     0x100U /* 64 words */
#define EEPROM_LUT_SLOTS    6


ret_t EEPROM_Write(uint32_t address, const uint32_t* pData, uint32_t words);
ret_t EEPROM_Read(uint32_t address, uint32_t* pData, uint32_t words);

#endif /* EEPROM_H_INCLUDED */

#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED

#include "test.h"
#include <stdint.h>

typedef enum ret_t
{
    RET_OK = 0,
    RET_FAILURE = 1,
    RET_INVALID_ARGUMENT = 2
} ret_t;

typedef enum input_mode_t
{
    INPUT_TAP = 0,
    INPUT_POT = 1
} input_mode_t;

extern volatile uint16_t g_tap_tempo;
extern volatile uint16_t g_pot_tempo;
extern volatile uint16_t g_led_counter;
extern volatile struct test_signal g_test_signal;
extern struct configuration g_config;
extern input_mode_t g_input_mode;

#endif /* COMMON_H_INCLUDED */

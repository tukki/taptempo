#ifndef DELAY_H_INCLUDED
#define DELAY_H_INCLUDED

#include "common.h"

ret_t Delay_Set(uint16_t delay_ms);

#endif /* DELAY_H_INCLUDED */

#ifndef CONFIG_H_INCLUDED
#define CONFIG_H_INCLUDED

#include "common.h"

#define CONFIG_LUT_SLOT_MAX         5
#define CONFIG_LUT_SLOT_DEFAULT     0
#define CONFIG_DELAY_SELECT_MAX     5
#define CONFIG_DELAY_SELECT_DEFAULT 0

struct __packed configuration
{
    uint8_t lut_slot;     /* 0 to 5 */
    uint8_t delay_select;   /* 0 to 5 */
};

ret_t Config_Load(void);
ret_t Config_Save(void);

#endif /* CONFIG_H_INCLUDED */

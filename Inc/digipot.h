#ifndef DIGIPOT_H_INCLUDED
#define DIGIPOT_H_INCLUDED

#include "common.h"

ret_t Digipot_EnableUpdate(void);

ret_t Digipot_WriteWiper(uint16_t value);
uint16_t Digipot_ReadWiper(void);



#endif /* DIGIPOT_H_INCLUDED */
